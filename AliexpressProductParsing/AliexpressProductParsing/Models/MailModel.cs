﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AliexpressProductParsing.Models
{
    public class MailModel
    {
        public MailModel()
        {
            From = "Admin";
            To = "admin@gmail.com";
            Subject = "Quantity Drop Alert";
            Body = "Quantity for the product _productName dropped from _histqty to _newqty";
        }
        public string From
        {
            get;
            set;
        }
        public string To
        {
            get;
            set;
        }
        public string Subject
        {
            get;
            set;
        }
        public string Body
        {
            get;
            set;
        }
    }
}