﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Windows.Forms;

namespace AliexpressProductParsing.Helpers
{
    public static class HTMLContent
    {
       

        [STAThread]
        public static HtmlDocument GetHtmlAjax(string uri, int AjaxTimeLoadTimeOut)
        {
            using (WebBrowser wb = new WebBrowser())
            {
                wb.Navigate(uri);
                while (wb.ReadyState != WebBrowserReadyState.Complete)
                    Application.DoEvents();
                Thread.Sleep(AjaxTimeLoadTimeOut);
                Application.DoEvents();
                return wb.Document;
            }
        }
    }
}