﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Web.UI;

namespace AliexpressProductParsing.Helpers
{
    public class MvcStaThreadHttpAsyncHandler : Page, IHttpAsyncHandler, IRequiresSessionState
    {
        RequestContext reqContext;

        public MvcStaThreadHttpAsyncHandler(RequestContext requestContext)
        {
            if (requestContext == null)
                throw new ArgumentNullException("requestContext");

            reqContext = requestContext;
        }

        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, object extraData)
        {
            return this.AspCompatBeginProcessRequest(context, cb, extraData);
        }

        protected override void OnInit(EventArgs e)
        {
            var controllerName = reqContext.RouteData.GetRequiredString("controller");
            if (string.IsNullOrEmpty(controllerName))
                throw new InvalidOperationException("Could not find controller to execute");

            var controllerFactory = ControllerBuilder.Current.GetControllerFactory();

            IController controller = null;
            try
            {
                controller = controllerFactory.CreateController(reqContext, controllerName);
                if (controller == null)
                    throw new InvalidOperationException("Could not find controller: " + controllerName);

            }
            catch
            {
                throw new InvalidOperationException("Could not find controller: " + controllerName +
                                                    ". Could be caused by missing default document in virtual.");
            }

            try
            {
                controller.Execute(reqContext);
            }
            finally
            {
                controllerFactory.ReleaseController(controller);
            }

            this.Context.ApplicationInstance.CompleteRequest();
        }

        public void EndProcessRequest(IAsyncResult result)
        {
            this.AspCompatEndProcessRequest(result);
        }

        public override void ProcessRequest(HttpContext httpContext)
        {
            throw new NotSupportedException();
        }
    }
}
