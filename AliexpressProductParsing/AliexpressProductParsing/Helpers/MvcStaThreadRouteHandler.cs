﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace AliexpressProductParsing.Helpers
{
    public class MvcStaThreadRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            if (requestContext == null)
                throw new ArgumentNullException("requestContext");

            return new MvcStaThreadHttpAsyncHandler(requestContext);
        }
    }
}