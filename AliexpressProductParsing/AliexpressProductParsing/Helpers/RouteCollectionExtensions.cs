﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace AliexpressProductParsing.Helpers
{
    public static class RouteCollectionExtensions
    {

        public static void MapMvcStaRoute(this RouteCollection routeTable,
                                     string name,
                                     string url,
                                     object defaults = null)
        {
            Route mvcRoute = new Route(url,
                                       new RouteValueDictionary(defaults),
                                       new MvcStaThreadRouteHandler());
            RouteTable.Routes.Add(mvcRoute);
        }
    }
}