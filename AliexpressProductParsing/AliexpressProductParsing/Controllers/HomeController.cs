﻿using AliexpressProductParsing.Helpers;
using AliexpressProductParsing.Models;
using HtmlAgilityPack;
using HtmlBuilders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Gecko;
using System.Windows.Forms;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Net.Mail;

namespace AliexpressProductParsing.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            AliExpressProductsEntities d = new AliExpressProductsEntities();
            var products = d.ImportedProducts.ToList();
            return View(products);
        }
        [HttpPost]
        public  ActionResult Index(string url)
        {
            

            HtmlWeb web = new HtmlWeb();
            var htmlDoc = web.Load(url);



            AliExpressProductsEntities d = new AliExpressProductsEntities();
            ImportedProduct m = new ImportedProduct();
            ProductImage pimg = new ProductImage();
            string price = string.Empty;
            string Name = string.Empty;
            string descritpion = string.Empty;
            string qty = string.Empty;
            string orignalprice = string.Empty;
            htmlDoc.OptionEmptyCollection = true;

            var htmlNodes = htmlDoc.DocumentNode.SelectNodes("//span");


            foreach (var node in htmlNodes)
            {
                if (node.Attributes["id"] != null)
                {
                    string cssclass = node.Attributes["id"].Value;
                    if (cssclass == "j-sku-discount-price")
                    {
                        if( node.InnerText!=null)
                        {
                            m.GivenPrice = Convert.ToDouble(node.InnerText);
                        }
                    }
                    if (cssclass == "j-sku-price")
                    {
                        if (node.InnerText != null)
                        {
                            m.ActualPrice = Convert.ToDouble(node.InnerText);
                        }
                    }
                }

            }
            var htmlEmNodes = htmlDoc.DocumentNode.SelectNodes("//em");
            foreach (var n in htmlEmNodes)
            {
                if (n.Attributes["id"] != null)
                {
                    string id = n.Attributes["id"].Value;
                    if (id == "j-sell-stock-num")
                    {
                        if (n.InnerText == null|| n.InnerText=="")
                        {
                            
                        }
                        else
                        {
                            m.Quantity = Convert.ToInt32(n.InnerText);
                        }
                    }
                }
                
            }
            var productNameNode = htmlDoc.DocumentNode.SelectSingleNode("//h1[@class='product-name']");
            var ProductDescription = htmlDoc.DocumentNode.SelectSingleNode("//div[@class='description-content']");
            var allimages = htmlDoc.DocumentNode.SelectNodes("//img[@alt='" + productNameNode.InnerText + "']");

            m.productDescription = ProductDescription.InnerHtml;
            m.ProductName = productNameNode.InnerHtml;
            m.URL = url;
            m.CreatedOn = DateTime.Now;
            d.ImportedProducts.Add(m);
            d.SaveChanges();
            int pid = m.ProductId;
            foreach(var im in allimages)
            {
                if (im.Attributes["src"] != null)
                {
                    pimg.imageUrl = im.Attributes["src"].Value;
                    pimg.productId = pid;
                    pimg.createdOn = DateTime.Now;
                    d.ProductImages.Add(pimg);
                    d.SaveChanges();
                }

                }
            chormeDriverForPriceDesc(url, pid);
            
            return View(d.ImportedProducts.ToList());
        }
        public void chormeDriverForPriceDesc(string url,int pid)
        {
            var co = new ChromeOptions();
            AliExpressProductsEntities d = new AliExpressProductsEntities();
            co.AddArgument("headless");
            co.AcceptInsecureCertificates = true;
            co.PageLoadStrategy = PageLoadStrategy.Normal;

            using (var driver = new ChromeDriver(co))
            {
                try
                {
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.Navigate().GoToUrl(url);
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));

                    wait.Until(wt => !string.IsNullOrWhiteSpace(wt.FindElement(By.Id("j-sell-stock-num")).Text));

                    var stock = driver.FindElementById("j-sell-stock-num");
                    var pdesc = driver.FindElementByClassName("description-content");
                    var product = d.ImportedProducts.Where(g => g.ProductId == pid).FirstOrDefault();

                    if (stock == null)
                    {
                        product.Quantity = 0;
                    }
                    else
                    {
                      
                        string qty = stock.Text;
                        int qtyfresh= Convert.ToInt32(qty.Replace("pieces", "").Replace(" ", ""));
                        if(qtyfresh<=10)
                        {
                            MailModel _objModelMail = new MailModel();
                            MailMessage mail = new MailMessage();
                            mail.To.Add(_objModelMail.To);
                            mail.From = new MailAddress(_objModelMail.From);
                            mail.Subject = _objModelMail.Subject;
                            string Body = _objModelMail.Body.Replace("_productName",product.ProductName).Replace("_histqty",Convert.ToString(product.Quantity)).Replace("_newqty", qty);
                            mail.Body = Body;
                            mail.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.gmail.com";
                            smtp.Port = 587;
                            smtp.UseDefaultCredentials = false;
                            smtp.Credentials = new System.Net.NetworkCredential("username", "password"); // Enter seders User name and password  
                            smtp.EnableSsl = true;
                            smtp.Send(mail);

                        }
                        product.Quantity = Convert.ToInt32(qty.Replace("pieces", "").Replace(" ",""));
                        if (pdesc ==null)
                        {

                        }
                        else
                        {
                            product.productDescription = pdesc.Text;
                        }
                       
                        
                    }
                    d.SaveChanges();
                    driver.Close();
                }
                catch (Exception ex)
                {
                    try
                    {
                        var screenshot = driver as ITakesScreenshot;
                        var pic = screenshot.GetScreenshot();
                        pic.SaveAsFile(@"C:/screenshot.png", ScreenshotImageFormat.Png);
                    }
                    catch (Exception ex2)
                    {
                        throw;
                    }
                }
            }
        }
        [HttpGet]
        public ActionResult UpdateInventory(int id)
        {
            AliExpressProductsEntities d = new AliExpressProductsEntities();
            var product = d.ImportedProducts.Where(g => g.ProductId == id).FirstOrDefault();
            chormeDriverForPriceDesc(product.URL, id);

            return View("Index", d.ImportedProducts);

        }

        [HttpGet]
        public ActionResult View(int id)
        {
            AliExpressProductsEntities d = new AliExpressProductsEntities();
            var product = d.ImportedProducts.Where(g => g.ProductId == id).FirstOrDefault();
            var productimages = d.ProductImages.Where(h => h.productId == id).ToList();
            product.ProductImage = productimages;
            

            return View(product);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        public ActionResult RefreshData(string[] ids)
        {
            AliExpressProductsEntities d = new AliExpressProductsEntities();
            foreach (string i in ids)
            {
                int id = Convert.ToInt32(i);

                var product = d.ImportedProducts.Where(g => g.ProductId == id).FirstOrDefault();
                chormeDriverForPriceDesc(product.URL, product.ProductId);



            }

            return Json(new { msg = "Updated"});

        }
    }
}